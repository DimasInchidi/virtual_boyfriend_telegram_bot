from telethon import events
from telethon.tl import functions
from telethon.tl.types import ChannelParticipantsAdmins, ChannelParticipantAdmin

from purge_group.models import PurgeRequests


async def is_admin_and_can_delete(client, chat, user):
    async for participant in client.iter_participants(chat, filter=ChannelParticipantsAdmins):
        if participant.id == user.id:
            if isinstance(participant.participant, ChannelParticipantAdmin):
                return participant.participant.admin_rights.delete_messages
            return True
    return False


async def purge_group(event):
    if event.is_group and event.is_channel:
        if not await is_admin_and_can_delete(event.client, event.chat, await event.client.get_me()):
            await event.reply('I do not have delete permission in this group.')
        else:
            message_sent = await event.reply('Retrieving messages to purge...')
            result = await event.client(functions.channels.GetFullChannelRequest(
                channel=event.chat_id
            ))
            pinned_msg_id = result.full_chat.pinned_msg_id
            msgs = []
            async for message in event.client.iter_messages(event.chat_id):
                if event.reply_to_msg_id and message.id >= event.reply_to_msg_id:
                    continue
                if pinned_msg_id and message.id == pinned_msg_id:
                    continue
                msgs.append(message)
            await message_sent.edit(f'Ready to delete {len(msgs)} message{"s" if len(msgs) > 1 else ""}.')
            await event.client.delete_messages(event.chat_id, msgs)
            query = PurgeRequests.delete().where(PurgeRequests.group_id == event.chat_id)
            query.execute()
            await event.reply(f'Purge complete, {len(msgs)} message{"s" if len(msgs) > 1 else ""} deleted.')
    else:
        await event.reply('This is not a super group chat, i will do nothing.')


@events.register(events.NewMessage(incoming=True, pattern='^#purge$', forwards=False))
async def purge_group_handler(event: events.NewMessage.Event):
    if await is_admin_and_can_delete(event.client, event.chat, event.message.sender):
        await purge_group(event)
    else:
        await event.reply('You do not have delete permission in this group.')


@events.register(events.NewMessage(incoming=True, pattern='^#request_purge$', forwards=False))
async def request_purge_group_handler(event: events.NewMessage.Event):
    if PurgeRequests.select().where(
            (PurgeRequests.group_id == event.chat_id)
            & (PurgeRequests.requested_by == event.message.from_id)
    ).exists():
        purge_request_count = PurgeRequests.select().where(PurgeRequests.group_id == event.chat_id).count()
        await event.reply(
            f'Purge request already saved with total '
            f'{purge_request_count} request{"s" if purge_request_count > 1 else ""}.'
        )
    elif PurgeRequests.select().where(
            (PurgeRequests.group_id == event.chat_id)
    ).count() == 3 - 1:
        await purge_group(event)
    else:
        PurgeRequests.create(
            group_id=event.chat_id,
            requested_by=event.message.from_id
        )
        purge_request_count = PurgeRequests.select().where(PurgeRequests.group_id == event.chat_id).count()
        await event.reply(
            f'Purge request saved with total '
            f'{purge_request_count} request{"s" if purge_request_count > 1 else ""}.'
        )


@events.register(events.NewMessage(incoming=True, pattern='^#cancel_purge$', forwards=False))
async def cancel_request_purge_group_handler(event: events.NewMessage.Event):
    if PurgeRequests.select().where(
            (PurgeRequests.group_id == event.chat_id)
            & (PurgeRequests.requested_by == event.message.from_id)
    ).exists():
        query = PurgeRequests.delete().where(
            (PurgeRequests.group_id == event.chat_id)
            & (PurgeRequests.requested_by == event.message.from_id)
        )
        query.execute()
        purge_request_count = PurgeRequests.select().where(PurgeRequests.group_id == event.chat_id).count()
        await event.reply(
            f'Purge request canceled. Current total request is '
            f'{purge_request_count} request{"s" if purge_request_count > 1 else ""}.'
        )
    else:
        await event.reply('You don\'t have purge request yet.')


@events.register(events.NewMessage(incoming=True, pattern='^#clear_purge$', forwards=False))
async def clear_request_purge_group_handler(event: events.NewMessage.Event):
    if await is_admin_and_can_delete(event.client, event.chat, event.message.sender):
        query = PurgeRequests.delete().where(PurgeRequests.group_id == event.chat_id)
        query.execute()
        await event.reply('All purge request cleared.')
    else:
        await event.reply('You do not have delete permission in this group.')


@events.register(events.NewMessage(incoming=True, chats=(1001485370320,), pattern='^#spam$'))
async def spam_group_handler(event: events.NewMessage.Event):
    for i in range(150):
        await event.client.send_message(event.chat_id, f'{i}')
