from peewee import IntegerField

from database import BaseModel


# class PurgeGroup(BaseModel):
#     group_id = IntegerField()
#     request_count = IntegerField()
#     request_max = IntegerField()


class PurgeRequests(BaseModel):
    group_id = IntegerField()
    requested_by = IntegerField()
