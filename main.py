#!/usr/bin/env python3
import logging

from decouple import config
from telethon import TelegramClient

from database import db
from purge_group.handlers import (
    purge_group_handler, spam_group_handler, request_purge_group_handler, cancel_request_purge_group_handler,
    clear_request_purge_group_handler
)
from purge_group.models import PurgeRequests
from utilities.handlers import (
    get_group_id_handler, mention_all_handler, bot_help_handler
)

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('telethon').setLevel(level=logging.WARNING)
logging.getLogger('peewee').setLevel(level=logging.WARNING)
logger = logging.getLogger(__name__)

USER_PHONE_NUMBER = config('USER_PHONE_NUMBER')


def initialize_db():
    db.create_tables(
        (
            PurgeRequests,
        ),
        safe=True
    )


with TelegramClient(config('SESSION_NAME'), config('API_ID', cast=int), config('API_HASH')) as client:
    initialize_db()
    print('(Press Ctrl+C to stop this)')
    client.add_event_handler(get_group_id_handler)
    client.add_event_handler(mention_all_handler)
    client.add_event_handler(bot_help_handler)
    client.add_event_handler(purge_group_handler)
    client.add_event_handler(request_purge_group_handler)
    client.add_event_handler(cancel_request_purge_group_handler)
    client.add_event_handler(clear_request_purge_group_handler)
    client.add_event_handler(spam_group_handler)
    client.run_until_disconnected()
