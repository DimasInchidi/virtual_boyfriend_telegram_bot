from datetime import datetime

from peewee import SqliteDatabase, Model, DateTimeField

db = SqliteDatabase('virtual_boyfriend_user_bot.sqlite3')


class BaseModel(Model):
    created = DateTimeField(default=datetime.now)

    class Meta:
        database = db
