from telethon import events


@events.register(events.NewMessage(incoming=True, pattern='@all'))
async def mention_all_handler(event: events.NewMessage.Event):
    mentions = ""
    async for x in event.client.iter_participants(event.chat_id, 100):
        mentions += f"[\u2063](tg://user?id={x.id})"
    await event.reply(mentions)
