from telethon import events


@events.register(events.NewMessage(incoming=True, pattern='#get_group_id'))
async def get_group_id_handler(event: events.NewMessage.Event):
    await event.reply(f'This group id: `{event.chat_id}`')
