from telethon import events


@events.register(events.NewMessage(incoming=True, pattern='#help'))
async def bot_help_handler(event: events.NewMessage.Event):
    await event.reply(
        f'''Available commands:

• `#help` : show this help
• `#purge` : purge all (or **until** replied message) message/s in group [admin with delete permission only]
• `#request_purge` : request to purge group (need 3 requests) [all group member]
• `#cancel_purge` : cancel your purge request
• `#clear_purge` : clear all purge request [admin with delete permission only]
• `#get_group_id` : i'll tell you this group id
• `@all` : mention all group members
 '''
    )
